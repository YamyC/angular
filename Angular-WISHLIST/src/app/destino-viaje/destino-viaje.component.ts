import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { DestinoViaje } from '../models/Destino-viaje.models';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @HostBinding('attr.class') cssclass = 'col-md-4';
  @Input() destino: DestinoViaje;
 constructor() {}

  ngOnInit(): void {
  }

}
